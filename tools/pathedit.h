/*
 * libkarma/pathedit.h
 *
 * Pathname editing
 *
 * Copyright (c) Enrique Vidal 2005, adapted from "tr.c", 
 * Copyright (c) ams@wiw.org (Abhijit Menon-Sen) 1998.
 *
 * You may distribute and modify this program under the terms of 
 * the GNU GPL, version 2 or later.
 */
#ifndef _PATHEDIT_H
#define _PATHEDIT_H

void tr(char *str, char *orig, char *sub, char *del, int squeezeRep);

#endif
