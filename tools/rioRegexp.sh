#!/bin/bash
rioDB=`ls ~/.openrio/pearl-*/fileinfo | head -1`
cat $rioDB |
grep -v profile= |
awk '!/^ *$/{printf "%s #_# ", $0}/^ *$/{printf "\n"}' |
egrep $* |
sed s'/ #_# /\
/g'
