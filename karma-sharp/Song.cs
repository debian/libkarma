using System;
using System.IO;
using System.Collections;
using System.Runtime.InteropServices;

using Mono.Unix.Native;

namespace Karma {

    public class Song {
        int fid;

        [DllImport ("karma")]
        private static extern IntPtr lk_properties_get_property(int fid, string key);
        public Song(int fid) {
            this.fid = fid;
        }

        private string GetString(string key) {
            return Marshal.PtrToStringAnsi(
                lk_properties_get_property(fid, key));
        }

        private int GetInt(string key) {
            try {
                string tmp = GetString(key);
                return (tmp == null) ? 0 : Int32.Parse(tmp);
            } catch (FormatException e) {
                return 0;
            }
        }

        public string Album {
            get { return GetString("source"); }
        }

        public string Artist {
            get { return GetString("artist"); }
        }

        public string Bitrate {
            get { return GetString("bitrate"); }
        }

        public string Codec {
            get { return GetString("codec"); }
        }

        public string Genre {
            get { return GetString("genre"); }
        }

        public int Id {
            get { return fid; }
        }

        public int Duration {
            get { return GetInt("duration") * 10; }
        }

        public string Title {
            get { return GetString("title"); }
        }

        public string Type {
            get { return GetString("type"); }
        }

        public uint TrackNumber {
            get { return (uint) GetInt("tracknr"); }
        }

        public uint PlayCount {
            get { return (uint) GetInt("play_count"); }
        }

        public DateTime LastPlayed {
            get { 
               return NativeConvert.ToDateTime((long)GetInt("play_last"));
            }
        }

        public int Year {
            get { return GetInt("year"); }
        }
    }
}
