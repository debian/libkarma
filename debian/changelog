libkarma (0.1.2-11) unstable; urgency=medium

  * QA upload.
  * debian/control: Update binary dependency to use libtag-c-dev
    as well.
  * debian/rules: Drop now useless dh_missing --fail-missing.

 -- Boyuan Yang <byang@debian.org>  Sat, 11 Jan 2025 13:44:58 -0500

libkarma (0.1.2-10) unstable; urgency=medium

  * QA upload.
  * Rebuild against taglib 2.0.2.
  * debian/control: Update build-dep: libtagc0-dev => libtag-c-dev.

 -- Boyuan Yang <byang@debian.org>  Fri, 10 Jan 2025 22:23:18 -0500

libkarma (0.1.2-9) unstable; urgency=medium

  * QA upload.
  * Drop cil packages.
  * d/copyright: Convert to machine-readable format.

 -- Bastian Germann <bage@debian.org>  Fri, 25 Aug 2023 15:27:21 +0000

libkarma (0.1.2-8) unstable; urgency=medium

  * QA upload.

  [ Debian Janitor ]
  * Update standards version to 4.6.1, no changes needed.
  * Avoid explicitly specifying -Wl,--as-needed linker flag.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sun, 27 Nov 2022 21:23:44 +0000

libkarma (0.1.2-7) unstable; urgency=medium

  * QA upload.

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster:
    + Build-Depends-Indep: Drop versioned constraint on cli-common-dev and
      mono-devel.
    + libkarma-cil-dev: Drop versioned constraint on libkarma-cil in Replaces.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Fri, 27 May 2022 20:10:17 +0100

libkarma (0.1.2-6) unstable; urgency=medium

  * QA upload.
  * Fix broken expansion of makefile variables that I introduced
    in the previous upload.

 -- Niels Thykier <niels@thykier.net>  Fri, 17 Apr 2020 20:31:32 +0000

libkarma (0.1.2-5) unstable; urgency=medium

  * QA upload.

  [ Debian Janitor ]
  * Update standards version, no changes needed.
  * Bump debhelper from old 11 to 12.
  * Update standards version to 4.4.1, no changes needed.

  [ Niels Thykier ]
  * Bump Standards-Version to 4.5.0 - no changes needed.
  * Set Rules-Requires-Root to no after passing relevant flags
    to upstream make file to avoid using root while installing
    the package.
  * Bump debhelper-compat to 13.
  * Add exclude to dh_clean to avoid removing files included in
    the upstream tarball.

  [ Helmut Grohne ]
  * Fix FTCBFS: Split out Build-Depends-Indep again. (Closes: #918477)

 -- Niels Thykier <niels@thykier.net>  Fri, 17 Apr 2020 20:18:40 +0000

libkarma (0.1.2-4) unstable; urgency=medium

  * QA upload.
  * debian/control:
    + Merge Build-Dep-Indep and Build-Dep to simplify dependency
      relationship and fix FTBFS on Debian Buildd.
  * debian/patches:
    + Apply patch to make package cross buildable. (Closes: #901265)

 -- Boyuan Yang <byang@debian.org>  Tue, 01 Jan 2019 18:36:36 -0500

libkarma (0.1.2-3) unstable; urgency=medium

  * QA upload.
  * Rebuild for Debian Buster.
  * debian/control:
    + Bump Standards-Version to 4.3.0.
    + Bump debhelper compat to v11.
    + Build-depend on debhelper-compat (= 11) instead of using
      debian/compat file.
    + Bump versioned build-dependency on cli-common-dev to
      (>= 0.8~) as indicated in buildlog.
    + Replace obsoleted Priority: extra with Priority: optional.
    + Add Vcs-* fields for packaging repository.
    + Update homepage field with secure uri.
    + Fix the section info for package libkarma-cil-dev (cli-mono).
  * debian/watch: Use secure uri.
  * debian/copyright: Remove trailing empty line.
  * debian/rules:
    + Use "dh_missing --fail-missing".
    + Force non-parallel build to fix FTBFS.

 -- Boyuan Yang <byang@debian.org>  Sun, 30 Dec 2018 20:27:34 +0800

libkarma (0.1.2-2.5) unstable; urgency=medium

  * Non-maintainer upload.
  * Fix FTBFS when building only arch-indep packages. Implement the
    workaround documented in #830416: when dh_makeshlibs has not been
    called, create a phony shlibs.local so that dh_clideps does not crash.
    (Closes: #806629)

 -- Sébastien Villemot <sebastien@debian.org>  Fri, 16 Dec 2016 22:36:06 +0100

libkarma (0.1.2-2.4) unstable; urgency=medium

  * Non-maintainer upload.
  * Rebuild for CLR 4.5 transition (Closes: #805464)

 -- Jo Shields <directhex@apebox.org>  Wed, 18 Nov 2015 12:22:18 +0000

libkarma (0.1.2-2.3) unstable; urgency=low

  * Non-maintainer upload
  * Rebuild for CLR 4.0 transition (Closes: #656379)
  * Drop deprecated cli.make include

 -- Chow Loong Jin <hyperair@debian.org>  Thu, 19 Jan 2012 04:59:04 +0800

libkarma (0.1.2-2.2) unstable; urgency=low

  * Non-maintainer upload.
  * Fix installing of karma-sharp dll when gmcs is not present (Closes: #634203)

 -- Chow Loong Jin <hyperair@ubuntu.com>  Mon, 18 Jul 2011 01:13:21 +0800

libkarma (0.1.2-2.1) unstable; urgency=low

  * Non-maintainer upload.
  * Fix build on non-mono architectures (Closes: #632770)
  * Use Debian's default C# compiler (mono-csc)
    Thanks to Julian Taylor <jtaylor.debian@googlemail.com> (Closes: #627299)

 -- Chow Loong Jin <hyperair@ubuntu.com>  Wed, 06 Jul 2011 04:55:06 +0800

libkarma (0.1.2-2) unstable; urgency=low

  * upload to unstable

 -- Joe Nahmias <jello@debian.org>  Sat, 23 Apr 2011 22:04:02 -0400

libkarma (0.1.2-1) experimental; urgency=low

  * New upstream release
    + 05_add_karma-sharp_dllmap: removed, applied upstream
    + 15_install_docs: removed, applied upstream
    + 25_update_manpages: removed, applied upstream
    + 50_add_write_smalldb_debug_info: removed, applied upstream
    + 80_gmcs: removed, applied upstream
    + 90_fix_debbug_567693: removed, applied upstream
  * debian/control: bump std-ver to 3.9.2, no changes required

 -- Joe Nahmias <jello@debian.org>  Fri, 22 Apr 2011 13:55:57 -0400

libkarma (0.1.1-3) experimental; urgency=low

  * control: move cli-common-dev from B-D-I to B-D, really closes:
    #614772.

 -- Joe Nahmias <jello@debian.org>  Tue, 15 Mar 2011 21:18:58 -0400

libkarma (0.1.1-2) experimental; urgency=low

  * Don't use --with cli in clean target, closes: #614772
  * improve karma-tools long description, closes: #515109.
  * 20_install_playlist_show_as_example: removed, part went upstream,
    rest went into debian/libkarma-dev.examples; closes: #547304.

 -- Joe Nahmias <jello@debian.org>  Thu, 24 Feb 2011 00:15:49 -0500

libkarma (0.1.1-1) experimental; urgency=low

  * New upstream release, closes: #427401.
    + 05_add_karma-sharp_dllmap: refresh
    + 51_fix_dmap_and_playlist_counts: included upstream
    + 52_check_for_upload_errors: included upstream
    + 60_fix_prop_count_off_by_ones: included upstream
    + 70_fix_prop-write-smalldb_retval: included upstream
    + 90_fix_debbug_567693: refresh
  * debian/watch: update upstream location
  * debian/control: bump std-ver to 3.9.1 -- no changes
  * drop dpatch and convert to source format 3.0 (quilt)
  * bump debian/compat to 7, minimize debian/rules using dh
  * link utilities using --as-needed

 -- Joe Nahmias <jello@debian.org>  Tue, 22 Feb 2011 19:33:21 -0500

libkarma (0.0.6-4.5) unstable; urgency=low

  * Non-maintainer upload.
  * debian/control, debian/rules: Move mono-devel back from Build-Depends to
    Build-Depends-Indep to allow libkarma to build on non-mono arches. Also
    make build depend on build-arch so that the bindings aren't built on all
    buildds. (Closes: #520862)
  * debian/control: Bump minimum version on mono-devel BDI to 2.4.3 and remove
    BDs on all packages provided by mono source package.

 -- Iain Lane <laney@ubuntu.com>  Sat, 17 Apr 2010 21:01:06 +0100

libkarma (0.0.6-4.4) unstable; urgency=low

  * Non-maintainer upload.
  * tools/karma_helper.c:
    + Only call usb_detach_kernel_driver_np() on Linux (Closes: #567693)
      (Thanks to Petr Salinger <Petr.Salinger@seznam.cz>)

 -- Jo Shields <directhex@apebox.org>  Mon, 12 Apr 2010 12:34:43 +0100

libkarma (0.0.6-4.3) unstable; urgency=low

  * Non-maintainer upload.
  * debian/control:
    + Bump mono-devel version to 2.4.2.3 for mono-csc
    + Create -cil-dev package containing pcfiles
  * debian/rules:
    + Use MCS=/usr/bin/mono-csc instead of /usr/bin/csc (Closes: #562259)
    + Add VERSION=0.0.6 to karma-sharp $(MAKE) invocations to ensure
      Version: field of .pc file is populated (Closes: #551049)

 -- Chow Loong Jin <hyperair@ubuntu.com>  Thu, 21 Jan 2010 19:31:18 +0800

libkarma (0.0.6-4.2) unstable; urgency=low

  * Non-maintainer upload.
  * Fix FTBFS on buildds: Move mono-devel from B-D-I to B-D as suggested
    by Peter Green, thanks! (Closes: #520862).

 -- Cyril Brulebois <kibi@debian.org>  Mon, 07 Dec 2009 06:20:39 +0100

libkarma (0.0.6-4.1) unstable; urgency=low

  * Non-maintainer upload.
  * debian/control:
    + Updated build-deps on Mono as part of the Mono 2.0 transistion.
      (thanks goes to James Westby for providing a patch)
  * debian/rules:
    + Override C# compiler variable.

 -- Mirco Bauer <meebey@debian.org>  Sat, 21 Mar 2009 14:28:30 +0100

libkarma (0.0.6-4) unstable; urgency=low

  * Ack NMU, closes: #458681, #423260, #459271.
  * Bump std-ver to 3.8.0, moved Homepage header to source section.
  * Bump debhelper compat to 6, no changes.
  * debian/rules: fix clean up of ./.wapi directory
  * debian/rules: split build & install targets into -arch and -indep
  * Improve tool manpages

 -- Joe Nahmias <jello@debian.org>  Fri, 27 Jun 2008 12:42:15 -0400

libkarma (0.0.6-3.1) unstable; urgency=low

  * Non-Maintainer upload.
  * debian/control:
    + Build depend on libmono2.0-cil (Closes: #458681).
  * debian/patches/05_add_karma-sharp_dllmap.dpatch:
    + Fix bashisms to fix build with dash (Closes: #423260).
      Thanks to Andrea Veri for the patch.
  * debian/patches/80_gmcs.dpatch:
    + Build with gmcs to only get 2.0 dependencies as the only depending
      package, banshee, already uses 2.0 and this will get use less
      dependencies (Closes: #459271).

 -- Sebastian Dröge <slomo@debian.org>  Sat, 05 Jan 2008 08:04:30 +0100

libkarma (0.0.6-3) unstable; urgency=low

  * Set MONO_SHARED_DIR per CLI Policy §4.3, closes: #445798.

 -- Joe Nahmias <jello@debian.org>  Tue, 16 Oct 2007 00:06:48 -0400

libkarma (0.0.6-2) unstable; urgency=low

  * Since libkarma-cil is managed code (c#) it should be arch:all.

 -- Joe Nahmias <jello@debian.org>  Sun, 14 Jan 2007 18:06:44 -0500

libkarma (0.0.6-1) unstable; urgency=low

  * Initial release, closes: #317173.
  * Added debian/watch file.
  * Wrote manpages for riocp, chprop, and karma_helper.
  * Install playlist_show as an example in libkarma-dev.
  * Install create_device-settings as an example in karma-tools.
  * Build karma-sharp dll for use with banshee.

 -- Joe Nahmias <jello@debian.org>  Mon,  8 Jan 2007 22:13:43 -0500
