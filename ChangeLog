0.1.2:
	* *bsd build fix
	* Use gmcs rather than mcs
	* add debugging info when writing the smalldb fails
	* Update manpages
	* Install documentation files.
	* Add dllmap for karma-sharp.dll to find libkarma.

0.1.1:
	* use DESTDIR/PREFIX instead of DEST in the Makefile
	* Fixed rio_rw.c create_temp_file/update_props_from_tags (LAN) bugs
	* Adapted karmaLan.c, karmaUsb.c and riocp.c to karma_delete_file change
	* Added lk_properties_del_property() into lk_karmaXXX_delete_file()
	* Added an HAL fdi file

0.1.0:
	* man files written by Joe Nahmias
	* removed the argument of lk_properties_cache_obsolete()
	* Added code to trim props trailing whitespace in set_tag_props/rio_rw.c
	* Added playlist-based print and download support in riocp
	* Updated Jamfiles and Makefiles and other build and install changes
	* Fixed props.count off-by-one bugs in andOrSearch and smalldb saving
	* Playlist functions added to lkarma.h

0.0.6:
	* Fixed null-bitrate-related segfaults
	* Fixed lk_mountSearch_discover when /proc/mounts > 1024 bytes
	* Don't check the device name of the mount in /proc/mounts
	* Changed MODE_PROPERTY from 'play_count_limit' to 'bpm'
	* Fixed bad length & duration yield by lk_rio_update_props_from_tags()
	* Added lk_rio_update_props_from_tags() to get correct rids in lkarmafs
	* Added support for calculating rids correctly in lkarmafs
	* Changed sorting tracks by artist to match "Rescan music" functionality
	* Changed device settings: don't use properties not held in smalldb
	* Re-implemented fdb support and made it functional
	* Fixed rio_rw error handling and several other bugs
	* Added lk_rio_update_props_from_tags() to help lkarmafs file upload
	* Get file type from its contents rather than the suffix.
	* Use the rid property to (optionally) avoid multiple uploaded copies

0.0.5:
	* Return an error during lk_karmaUsb_connect() if essential
	  directories (fids0 and var) are not found
	* Added regression tests
	* Removed half-finished fdb code
	* Moved library interface into a seperate header file, "lkarma.h"
	* Use function pointers to group the usb-or-net switch in one place
	* Added mountSearch and fixed karma.c and errors.c bugs
	* Indent and Whitespace cleanup
	* Makefile/Jamfile Added missing targets
	* Add karma-sharp to support the Banshee music player
	* riocp.c Options for smalldb management and dummy names
	* riocp.c Added dummy names for missing or blank properties
	* karmaUsb.c/properties.c/etc. Added smalldb read/write support
	* karmaUsb.c Improve and fix memory allocation in read_properties()
	* karmaUsb.c Substitute zeroes (null bytes) from xx1 files with blanks
	* properties.c Changed hash_insert() call in lk_properties_import()
	  (needed for missing properties AndOrSearch)
	* properties.c Fixed buggy STRMATCH() (needed for correct AndOrSearch)
	* fdb.c Fixed use of andOrSearch
	* Fixed OR Search (broken since libkarma-0.0.3 changeset 30)

0.0.4:
	* minor cleanup for the release of 0.0.4

0.0.4:-pre2:
	* Fixed lk_properties_import bug while parsing properties with value=""
	* Append "/" to the user-given usbMountPoint if necessary
	* Fixed a few potential memory faults in karmaUsb.c

0.0.4:-pre1:
	* First version with full support for mounted karma disk (USB/OMFS)
	* Changed karma.[ch], moved some functions to util.[ch] and created
	  karmaUsb.[ch] and KarmaLan[.ch] to support USB/OMFS operation
	* changed hash_create(32) -> hash_create(16); saves up to 300KB
	* Added fidTable[] to klist (and eliminated `struct list * l'),
	  for speeding up lk_properties_idsearch()
	* added support for EXACT AND/OR search and changed to EXACT all
	  the calls to lk_properties_andOrSearch() which needed to be EXACT
	* Fixed Makefiles to include taglib (and remove libFLAC/libvorbisfile)
	* moved pathname editing (tr) out of riocp.c and changed -q after del
	* fixed a bug of write_file_chunk/rio_rw (byte padding of files)
	* added taglib to support all major music tags: id3v2, ogg, flac...
	* moved the charset conversions out of libkarma
	* all functions are namespace renamed as `lk_file_function'
	* get_next_fid has been made faster
	* folders-based up/down-load fully implemented into fdb.[ch]
	  original pathnames are now saved as .openrio/*/libkarma_fdb.gz
	* CHANGELOG: 0.0.3 entries are now reverse time sorted

0.0.3:
	* src/playlist: preliminary support for playlist management
	* tools/riocp: tr-like editing of pathnames built from properties
	* tools/riocp-rio_rw: using properties to determine downlod paths
	* tools/riocp-rio_rw: deleting files
	* src/properties: add convertFromUtf8() and get_propertyUtf8()
	* src/properties: added a search functions
	* riocp/rio_rw: preliminary support of folders-based up/down-load
	  with full paths writen/read to/from .openrio/*/fidNamesFile
	* replaced nested function (not ansi c) with an equivalent ansi one
	  (Georg Sauthoff)
	* changed the caching mechanism to the openrio "standard"
	  (http://www.cliff.biffle.org/software/pearl/openrio.php)
	* path to the cache compatible to rmmlite (Michael Piotrowski)
	* Patches for NetBSD by Michael Piotrowski (but you have to use jam;))

0.0.2:
	* fixed karma.c for gcc 3.4
	* stripped rio_rw/mp3/wav from the lib
	* fix for hash_insert (Enrique Vidal)
	* implemented caching via load_properties,save_properties,
	  update_database
	* added ssdp support
	* jam support, see http://www.perforce.com/jam/jam.html
	* new Makefiles (Enrique Vidal)
	* fixed byte order in karma.c
	* error handling (errors.[ch]: Enrique Vidal)
	* utf8 supported in properties.c

0.0.1:
	* initial release
