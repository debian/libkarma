/*
 * libkarma/status.h
 *
 * Copyright (c) Frank Zschockelt <libkarma@freakysoft.de> 2005
 *
 *
 * You may distribute and modify this program under the terms of 
 * the GNU GPL, version 2 or later.
 * 
 */

#ifndef _STATUS_H
#define _STATUS_H

#include <inttypes.h>

typedef void status_func(const char * in_progress,int percent);

int lk_status_init(status_func update);
int lk_status_setup(const char * in_progress, uint32_t start, uint32_t units);
int lk_status_increment(uint32_t step);
int lk_status_destroy(void);

#endif /* _STATUS_H */
