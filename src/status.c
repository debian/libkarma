/*
 * libkarma/status.c
 *
 * Copyright (c) Frank Zschockelt <libkarma@freakysoft.de> 2005
 *
 *
 * You may distribute and modify this program under the terms of 
 * the GNU GPL, version 2 or later.
 * 
 */
#include <stdlib.h>

#include "status.h"

static status_func * callback = NULL;

int lk_status_init(status_func update)
{
    return 0;
}

int lk_status_setup(const char * in_progress, uint32_t start, uint32_t units)
{
    return 0;
}

int lk_status_increment(uint32_t step)
{
    return 0;
}

int lk_status_destroy(void)
{
    return 0;
}
