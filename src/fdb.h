/*
 * libkarma/fdb.h
 * 
 * Copyright (c) Frank Zschockelt <libkarma@freakysoft.de> 2005
 * 
 * You may distribute and modify this program under the terms of 
 * the GNU GPL, version 2 or later.
 * 
 */
#ifndef _FDB_H
#define _FDB_H

extern int fdb_updated;

int  lk_fdb_load      (int download);
int  lk_fdb_save      (void);
void lk_fdb_set_device(int karma);

#endif /* _FDB_H */
