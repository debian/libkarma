/*
 * libkarma/karma.h
 *
 * Copyright (c) Frank Zschockelt <libkarma@freakysoft.de> 2004
 *
 * You may distribute and modify this program under the terms of 
 * the GNU GPL, version 2 or later.
 *
 */

#ifndef _KARMA_USB_H
#define _KARMA_USB_H

#include <inttypes.h>

char *   lk_karmaUsb_fidToPath              (int rio, uint32_t file_id);

/*Help-Functions:*/
int      lk_karmaUsb_connect                (char *ip);

/*Basic Protocol Functions*/
uint32_t lk_karmaUsb_authenticate           (int rio, char *pass);
int      lk_karmaUsb_get_storage_details    (int rio, uint32_t storage_id,
                                             uint32_t *n_files,
                                             uint64_t *s_size,
                                             uint64_t *f_space,
                                             uint32_t *highest_file_id);
int      lk_karmaUsb_get_device_settings    (int rio);
int32_t  lk_karmaUsb_request_io_lock        (int rio, uint32_t type);
int32_t  lk_karmaUsb_release_io_lock        (int rio);
int32_t  lk_karmaUsb_write_file_chunk       (int rio, uint64_t offset,
                                             uint64_t size, uint32_t file_id,
                                             uint32_t storage_id,
                                             const char *data);
int32_t  lk_karmaUsb_get_all_file_details   (int rio, char **properties);
int32_t  lk_karmaUsb_update_file_details    (int rio, uint32_t file_id,
                                             char * properties);
int32_t  lk_karmaUsb_read_file_chunk        (int rio, uint64_t offset,
                                             uint64_t size, uint32_t file_id,
                                             char **data, uint64_t *retsize);
int32_t  lk_karmaUsb_delete_file            (int rio, uint32_t file_id);
int32_t  lk_karmaUsb_hangup                 (int rio);
int      lk_karmaUsb_write_smalldb          (void);

/*Advanced Protocol Functions:*/
void     lk_karmaUsb_load_database          (int rio);
void     lk_karmaUsb_load_database_smalldb  (int rio);

#endif /* _KARMA_USB_H */
