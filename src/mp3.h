/*
 * libkarma/mp3.h
 *
 * Copyright (c) Cedric Tefft <cedric@earthling.net> 2000-2001
 *           (c) Frank Zschockelt <libkarma@freakysoft.de> 2004
 *
 * You may distribute and modify this program under the terms of 
 * the GNU GPL, version 2 or later.
 *  
 ***************************************************************************
 * This file is based in part on:
 * MP3Info 0.5 by Ricardo Cerqueira <rmc@rccn.net>
 * MP3Stat 0.9 by Ed Sweetman <safemode@voicenet.com> and 
 *                Johannes Overmann <overmann@iname.com>
 *
 */

#ifndef _MP3_H
#define _MP3_H

/* MIN_CONSEC_GOOD_FRAMES defines how many consecutive valid MP3 frames
     we need to see before we decide we are looking at a real MP3 file */
#define MIN_CONSEC_GOOD_FRAMES 4
#define FRAME_HEADER_SIZE 4
#define MIN_FRAME_SIZE 21
#define NUM_SAMPLES 4

typedef struct {
    unsigned long   sync;
    unsigned int    version;
    unsigned int    layer;
    unsigned int    crc;
    unsigned int    bitrate;
    unsigned int    freq;
    unsigned int    padding;
    unsigned int    extension;
} mp3header;

typedef struct {
    char title[31];
    char artist[31];
    char album[31];
    char year[5];
    char comment[31];
    unsigned char track[1];
    unsigned char genre[1];
} id3tag;

typedef struct {
    char *filename;
    FILE *file;
    off_t datasize;
    int header_isvalid;
    mp3header header;
    int id3_isvalid;
    id3tag id3;
    int vbr;
    int vbr_average;
    int ms;
    int offset;
    int frames;
    int badframes;
} mp3info;

int get_mp3_info(mp3info *mp3);
int header_layer(mp3header *h);
int header_bitrate(mp3header *h);
int header_frequency(mp3header *h);
int write_tag(mp3info *mp3);

#endif /* _MP3_H */
