/*
 * libkarma/rio_rw.h
 *
 * Copyright (c) Frank Zschockelt <libkarma@freakysoft.de> 2004-2005
 * 
 * You may distribute and modify this program under the terms of 
 * the GNU GPL, version 2 or later.
 *
 */

#ifndef _RIO_RW_H
#define _RIO_RW_H

uint32_t lk_rio_write_fdb(int rio, const char * filename);

#endif /* _RIO_RW_H */
