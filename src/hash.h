/*
 * libkarma/hash.h
 *
 * Copyright (c) Michael R. Elkins <me@mutt.org> 1996-2000
 *           (c) Frank Zschockelt <libkarma@freakysoft.de> 2004
 *
 * You may distribute and modify this program under the terms of 
 * the GNU GPL, version 2 or later.
 *
 */

#ifndef _HASH_H
#define _HASH_H

struct hash_elem
{
    const char *key;
    void *data;
    struct hash_elem *next;
};

typedef struct
{
    int nelem;
    struct hash_elem **table;
}
HASH;

#define hash_find(table, key) \
    hash_find_hash(table, hash_string((unsigned char *)key, table->nelem), key)

#define hash_delete(table,key,data,destroy) \
    hash_delete_hash(table, hash_string((unsigned char *)key, table->nelem), \
                     key, data, destroy)

HASH * hash_create(int nelem);
int hash_string(const unsigned char *s, int n);
int hash_insert(HASH * table, const char *key, void *data);
int hash_count(HASH * table);
void hash_iterate(HASH * table, int index, const char **key, void **data);
void hash_walker(HASH * table, void (*fn)(const char *key, void *data));
void *hash_find_hash(const HASH * table, int hash, const char *key);
void hash_delete_hash(HASH * table, int hash, const char *key, const void *data);
void hash_destroy(HASH ** hash);

#endif /* _HASH_H */
