/*
 * libkarma/ssdp.h
 *
 * Copyright (c) Nicolas Justin+Gregrory Kokanosky 2004
 *           (c) Frank Zschockelt <libkarma@freakysoft.de> 2004
 *
 * You may distribute and modify this program under the terms of 
 * the GNU GPL, version 2 or later.
 *
 */
#ifndef _SSDP_H
#define _SSDP_H

#define RESPONSE_BUFFER_LEN 1024

#define SSDP_MULTICAST      "239.255.255.250"
#define SSDP_PORT           1900
    
#endif /* _SSDP_H */
